package LinkedList;

public class ListNode {
	Object element;
	ListNode next;
	
	ListNode(Object theElement, ListNode n) {
		element = theElement;
		next = n;
	}
	
	ListNode(Object theElement) {
		this(theElement, null);
	}
	
	ListNode() {
		this(null, null);
	}
}
