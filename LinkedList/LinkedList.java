package LinkedList;

public class LinkedList {
	
	/**
	 * Node yang tidak berisikan data, digunakan untuk menjamin bahwa selalu ada elemen sebelum
	 * elemen pertama yang sebenarnya pada linked list.
	 * Elemen pertama : current = header.next;
	 */
	ListNode header;
	
	public LinkedList() {
		header = new ListNode();
	}
	
//	public boolean add(Object element)
//	{
//		ListNode last = getLast();
//	}
	
	/**
	 * Insert the given element at the beginning of a list.
	 * @param element : element of the new node
	 */
	public void addFirst(Object element) 
	{
		ListNode tmp = new ListNode();
		tmp.element = element;
		tmp.next = header.next;
		header.next = tmp;
		
		// short : header.next = new ListNode(element, header.next);
	}
	
	/**
	 * Append the given element to the end of a list
	 * @param element : element of the new last node
	 */
	public void addLast(Object element) 
	{
		ListNode last = getLast();
		
		last.next = new ListNode();
		last = last.next;
		last.element = element;
		
		// short : last = last.next = new ListNode(element);
	}
	
	
//		
//	}
	

	/**
	 * Get the first node in the list
	 * @return first element
	 */
	public ListNode getFirst()
	{
		return header.next;
	}
	
	/**
	 * Get the last node in the list
	 * @return last node
	 */
	public ListNode getLast()
	{
		ListNode node = header.next;
		while (node != null) {
			node = node.next;
		}
		
		return node;
	}
	
	/**
	 * Return the number of elements in a list
	 * @return the number of elements in the list
	 */
	public int size()
	{
		int size = 0;
		ListNode node = getFirst();
		
		while (node != null) {
			node = node.next;
			size++;
		}
		
		return size;
	}

	/**
	 * Remove all of the elements (nodes) in the list
	 */
	public void clear()
	{
		header.next = null;
	}

	/**
	 * Check if the list is logically empty.
	 * @return true if empty, false otherwise
	 */
	public boolean isEmpty() {
		return header.next == null;
	}
	
}
